#include "TrigFTK_RecAlgs/TrigFTKFastSim.h"
#include "TrigFTK_RecAlgs/TrigFTK_VxPrimary.h"
#include "TrigFTK_RecAlgs/TrigFTK_VxPrimaryAllTE.h"

DECLARE_COMPONENT( TrigFTKFastSim )
DECLARE_COMPONENT( TrigFTK_VxPrimary )
DECLARE_COMPONENT( TrigFTK_VxPrimaryAllTE )
